---
title: "Spring Benefit Concert June 9"
date: 2022-05-10
showReadingTime: false
showDate: false
showSummary: true
---
Save the date! Our spring concert will be held on June 9, 7:30 pm at the Matsqui Auditorium in Abbotsford. We look forward to sharing some old favourites and new arrangements with our audience. Bring your friends and family for an evening of delightful choral music. It's good to be back!
 
Admission is by donation. All proceeds will go to a charity TBA.

