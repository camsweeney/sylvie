---
title: "Christmas Benefit Concert December 20"
date: 2022-12-18
showReadingTime: false
showDate: false
showSummary: true
---
![Sweeney Singers](thumb-sweeney-christmas-2022.png)
Now in their 34th season the award-winning Sweeney Singers continue to live up to
their reputation for producing a program of diverse choral music demonstrating
pure vocal sound, balanced blend and sensitive interpretation.
 

This entertaining Christmas program includes sacred and secular music, from early renaissance to contemporary, including classic, folk and jazz styles. The camaraderie of family plus musical excellence continues to draw an enthusiastic
and appreciative audience.


The Sweeneys are committed to serving their community - admission is by donation
with proceeds going to support the Archway Food Bank.

 

Tuesday, December 20, 2022 at 7:30 pm
Matsqui Centennial Auditorium
32315 South Fraser Way, Abbotsford
