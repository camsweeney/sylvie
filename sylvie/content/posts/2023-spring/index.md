---
title: "Spring Benefit Concert June 3"
date: 2023-04-23
showReadingTime: false
showDate: false
showSummary: true
---
![Sweeney Singers](images/sweeney-singers-banner.webp) 

June 3, 7:30 pm at Matsqui Auditorium, Abbotsford.

For over 3 decades the Sweeney Singers have been entertaining a loyal audience
with their fine choral music. Their primary goal has been to promote excellence in
music and to explore different genres, while also making it accessible to their
listeners. The ARTY award-winning group is excited to share music, old and new.
 

In keeping with the theme, Music Lives On, this concert includes sacred early music, choral jazz and folk music, highlighting Canadian composers and arrangers.
Featuring songs from the folk era, the singers present music arranged by
Abbotsford’s Larry Nickel by three Canadian icons, Ian Tyson, Gordon Lightfoot, and Rita McNeil.
 

Admission is by donation, with proceeds going to support FOOD FOR THE HUNGRY.
