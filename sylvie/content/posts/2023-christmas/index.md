---
title: "Christmas Benefit Concert December 19"
date: 2023-11-09
showReadingTime: false
showDate: false
showSummary: true
---
![Sweeney Singers](thumb-sweeney-singers-poster.png)
Our Christmas concert will be held on December 19, 2023 at [Matsqui Centennial Auditorium](https://www.google.com/maps?channel=fs&client=ubuntu-sn&um=1&ie=UTF-8&fb=1&gl=ca&sa=X&geocode=KUk7Tm5PNYRUMQwoINDeR3At&daddr=32315+South+Fraser+Way+1st+floor,+Abbotsford,+BC+V2T+1W7). 

This years concert moves through 15th Century Tomas Victoria, to Christmas folk tunes from Italy, Germany, Wales, and Scotland and popular secular tunes from the 40’s to today. 

We always enjoy singing together but especially when sharing our love of festive Christmas music. Bring your friends and family for an evening of delightful choral music.  

Admission is by donation, with proceeds going to support the [Abbotsford Archway Food
Bank](https://archway.ca/).
