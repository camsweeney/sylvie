---
title: "Spring Benefit Concert June 11"
date: 2024-06-05
showReadingTime: false
showDate: false
showSummary: true
---
![Sweeney Singers](thumb-sweeney-singers-2024.png)
Spring concert on June 11, 2024 7:30 pm at [Matsqui Centennial Auditorium](https://www.google.com/maps?channel=fs&client=ubuntu-sn&um=1&ie=UTF-8&fb=1&gl=ca&sa=X&geocode=KUk7Tm5PNYRUMQwoINDeR3At&daddr=32315+South+Fraser+Way+1st+floor,+Abbotsford,+BC+V2T+1W7).

The Sweeney Singers are pleased to be joined by four young members of the next Sweeney generation,
featuring selections from the Sound of Music, sacred and secular music, including Bach,
Mozart, Lennon & McCartney, and folk.

Praised for their mastery of diverse styles of music and pure, clear a capella singing, the
Sweeneys have been enjoyed by audiences for 35 years. Although music is their passion, they
continue to have a heart for others, with all proceeds passed on to various charities, locally and
internationally.

Admission is by donation, with proceeds going to [Help Burkina Faso Canada](https://www.helpburkinafasocanada.com/).
